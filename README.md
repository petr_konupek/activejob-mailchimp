# MailChimp + ActiveJob #

Jednoduchý MailChimp klient, využívající pro dávkové zpracování emailů ActiveJob

https://github.com/rails/rails/tree/master/activejob

Pro použití je nutné mít MailChimp profil:
https://login.mailchimp.com/signup?

Aplikace využívá API wrapper Gibbon
https://github.com/amro/gibbon

## Instalace ##

```
#!bash

git clone git@bitbucket.org:petr_konupek/activejob-mailchimp.git
cd activejob-mailchimp.git
bundle install
```

Nastavte MailChimp API klíče v:
**config/secrets.yml**

Enjoy ;)