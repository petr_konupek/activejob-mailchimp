class VisitorsController < ApplicationController

  def new
    @visitor = Visitor.new
  end

  def create
    @visitor = Visitor.new(params.require(:visitor).permit(:email))
    if @visitor.save
      redirect_to root_path, notice: "Prihlasen #{@visitor.email}."
    else
      render :new
    end
  end

end
