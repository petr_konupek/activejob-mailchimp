class MailingListSignupJob < ActiveJob::Base

  def perform(visitor)
    logger.info "Vkladam #{visitor.email}"
    visitor.subscribe
  end

end
